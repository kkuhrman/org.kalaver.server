# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Kalaver is a portmanteau of kaufen, lagern and verkaufen - the German verbs for buy, store, sell respectively. Kalaver is a Free Software inventory control system written in C. The Kalaver server is daemon which receives and responds to requests from user agents (clients) and also communicates with other Kalaver services (modules) on the network.
### How do I get set up? ###

Kalaver development has not started. The project is in the initial design phase. Check the website for news: https://kalaver.org.

### Contribution guidelines ###

Kalaver development has not started. The project is in the initial design phase. Check the website for news: https://kalaver.org.

### Who do I talk to? ###

Direct any questions about this project to Kuhrman Technology Solutions LLC info@kuhrman.com.